package com.retoglobant2021redis.omar.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 13/09/2021
 * Time: 6:50 p. m.
 * Change Text
 */
@Data
public class Student implements Serializable {

  private String name;
  private String lastname;
  private int age;
  private int status;
}
