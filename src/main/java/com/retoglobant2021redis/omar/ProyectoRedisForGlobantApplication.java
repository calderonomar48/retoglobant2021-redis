package com.retoglobant2021redis.omar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoRedisForGlobantApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoRedisForGlobantApplication.class, args);
	}

}
