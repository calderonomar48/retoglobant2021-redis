package com.retoglobant2021redis.omar.util;

import java.lang.annotation.*;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 14/09/2021
 * Time: 7:46 p. m.
 * Change Text
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface HttpHeaders {
}
