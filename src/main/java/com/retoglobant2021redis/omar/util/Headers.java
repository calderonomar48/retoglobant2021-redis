package com.retoglobant2021redis.omar.util;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 14/09/2021
 * Time: 7:42 p. m.
 * Change Text
 */
@Data
public class Headers {

  @NotNull(message = "es requerido en headers")
  private String idUser;

  @NotNull(message = "es requerido en headers")
  private String deviceToken;

}
