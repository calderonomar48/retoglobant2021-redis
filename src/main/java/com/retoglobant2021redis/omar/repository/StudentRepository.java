package com.retoglobant2021redis.omar.repository;

import com.retoglobant2021redis.omar.model.Student;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.UUID;


/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 13/09/2021
 * Time: 5:57 p. m.
 * Change Text
 */
@Repository
public class StudentRepository implements RedisRepository {

  // este KEy es para que REDIS identifique que tipo de objeto es
  private static final String KEY = "Student";

  private HashOperations hashOperations;

  private RedisTemplate<String, Student> redisTemplate;

  public StudentRepository(RedisTemplate<String, Student> redisTemplate) {
    this.redisTemplate = redisTemplate;
  }

  // sobreescribimos el metodo init para setear el objeto de hashOprations
  @PostConstruct
  private  void init(){
    hashOperations = redisTemplate.opsForHash();
  }

  @Override
  public Map<String, Student> findAll() {
    return hashOperations.entries(KEY);
  }

  @Override
  public Student findById(String id) {
    return (Student) hashOperations.get(KEY, id);
  }

  @Override
  public void save(Student student) {
    hashOperations.put(KEY, UUID.randomUUID().toString(), student);
  }

  @Override
  public void delete(String id) {
    hashOperations.delete(KEY, id);
  }

}
