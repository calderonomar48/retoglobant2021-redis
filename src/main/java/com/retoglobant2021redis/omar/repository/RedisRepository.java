package com.retoglobant2021redis.omar.repository;

import com.retoglobant2021redis.omar.model.Student;

import java.util.Map;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 13/09/2021
 * Time: 5:55 p. m.
 * Change Text
 */
public interface RedisRepository {

  Map<String, Student> findAll();

  Student findById(String id);

  void save(Student student);

  void delete(String id);

}
