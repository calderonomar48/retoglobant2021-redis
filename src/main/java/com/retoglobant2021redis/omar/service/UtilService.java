package com.retoglobant2021redis.omar.service;

import io.reactivex.Single;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 14/09/2021
 * Time: 7:35 p. m.
 * Change Text
 */
public interface UtilService {

  void setRedis(String cacheName, String idUser, String deviceToken);

  Single<String> getRedis(String id);

}
