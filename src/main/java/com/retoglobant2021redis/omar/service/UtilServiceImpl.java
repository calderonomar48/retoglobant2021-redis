package com.retoglobant2021redis.omar.service;

import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 14/09/2021
 * Time: 7:34 p. m.
 * Change Text
 */
@Service
public class UtilServiceImpl implements  UtilService{

  @Autowired
  private RedisTemplate<String, String> redisTemplate;

  @Override
  public Single<String> getRedis(String id) {
    return Single.create(subscribe -> {
      ValueOperations<String, String> values = redisTemplate.opsForValue();
      subscribe.onSuccess(values.get(id));
    });
  }

  @Override
  public void setRedis(String cacheName, String idUser, String deviceToken) {
    ValueOperations<String, String> values = redisTemplate.opsForValue();
    values.set(cacheName + idUser, deviceToken);
  }
}
