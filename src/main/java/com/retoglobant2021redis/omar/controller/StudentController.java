package com.retoglobant2021redis.omar.controller;

import com.retoglobant2021redis.omar.model.Student;
import com.retoglobant2021redis.omar.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 13/09/2021
 * Time: 6:49 p. m.
 * Change Text
 */
@RestController
public class StudentController {

  @Autowired
  private StudentRepository studentRepository;

  public StudentController(StudentRepository studentRepository) {
    this.studentRepository = studentRepository;
  }

  @GetMapping("/students")
  public Map<String, Student> findAll(){
    return studentRepository.findAll();
  }

  @GetMapping("/students/{id}")
  public Student findById(@PathVariable String id){
    return studentRepository.findById(id);
  }

  @PostMapping("/students")
  public void save(@RequestBody Student student){
    studentRepository.save(student);
  }

  @DeleteMapping("/students/{id}")
  public void delete(@PathVariable String id){
    studentRepository.delete(id);
  }

}
