package com.retoglobant2021redis.omar.controller;

import com.retoglobant2021redis.omar.model.Student;
import com.retoglobant2021redis.omar.service.UtilService;
import com.retoglobant2021redis.omar.util.Headers;
import com.retoglobant2021redis.omar.util.HttpHeaders;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Omar Calderon Evangelista
 * User: Omar.Calderon
 * Date: 14/09/2021
 * Time: 7:41 p. m.
 * Change Text
 */
@RestController
public class UtilsController {

  public static final String SESSION_TOKEN = "sessionToken:";

  @Autowired
  private UtilService utilService;

  @PostMapping(value = "/setCache",
               consumes = MediaType.APPLICATION_JSON_VALUE)
  public void setCache(@RequestBody Headers headers){
    utilService.setRedis(SESSION_TOKEN, headers.getIdUser(), headers.getDeviceToken());
  }

  @PostMapping(value = "/getCache/{id}",
          produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  public Single<String> getCache(@PathVariable String id){
    return utilService.getRedis(id);
  }
}
